var HERO_DATA_MAIN = [
    {
        image: "./resources/images/hero/bg_hero_0.png",
        title: "Мы делаем<br />видеоконтент,<br />который работает<span style=\"color: var(--c-green)\">.</span>",
        description: "Корпоративное, имиджевое и B2B видео:<br /><span class=\"b\">Сбербанк.Ecoestate</span>"
    },
    {
        image: "./resources/images/hero/bg_hero_1.png",
        title: "Мы делаем<br />видеоконтент,<br />который работает<span style=\"color: var(--c-green)\">.</span>",
        description: "Обучающее видео:<br /><span class=\"b\">Глеб Никитин о Великом Новгороде</span>"
    },
    {
        image: "./resources/images/hero/bg_hero_2.png",
        title: "Мы делаем<br />видеоконтент,<br />который работает<span style=\"color: var(--c-green)\">.</span>",
        description: "Реклама:<br /><span class=\"b\">Азерчай</span>"
    },
    {
        image: "./resources/images/hero/bg_hero_3.png",
        title: "Мы делаем<br />видеоконтент,<br />который работает<span style=\"color: var(--c-green)\">.</span>",
        description: "Документальное кино:<br /><span class=\"b\">Хой</span>"
    },
    {
        image: "./resources/images/hero/bg_hero_4.png",
        title: "Мы делаем<br />видеоконтент,<br />который работает<span style=\"color: var(--c-green)\">.</span>",
        description: "TV формат:<br /><span class=\"b\">Уроки русского</span>"
    },
    {
        image: "./resources/images/hero/bg_hero_5.png",
        title: "Мы делаем<br />видеоконтент,<br />который работает<span style=\"color: var(--c-green)\">.</span>",
        description: "2D и 3D графика:<br /><span class=\"b\">Наука.Кооперация</span>"
    },
    {
        image: "./resources/images/hero/bg_hero_6.png",
        title: "Мы делаем<br />видеоконтент,<br />который работает<span style=\"color: var(--c-green)\">.</span>",
        description: "Видеокомиксы:<br /><span class=\"b\">Академия регби</span>"
    },
    {
        image: "./resources/images/hero/bg_hero_7.png",
        title: "Мы делаем<br />видеоконтент,<br />который работает<span style=\"color: var(--c-green)\">.</span>",
        description: "Инфорграфика и анимация:<br /><span class=\"b\">Карантин для мусора</span>"
    },
];
var HERO_DATA_PROJECTS = [
    {
        image: "./resources/images/hero/bg_hero_0.png",
        title: "Корпоративное,<br />имиджевое<br />и B2B видео<span style=\"color: var(--c-green)\">.</span>",
        description: "Видеопроект:<br /><span class=\"b\">Сбербанк.Ecoestate</span>"
    },
    {
        image: "./resources/images/hero/bg_hero_1.png",
        title: "Обучающее<br />видео<span style=\"color: var(--c-green)\">.</span>",
        description: "Видеопроект:<br /><span class=\"b\">Глеб Никитин о Великом Новгороде</span>"
    },
    {
        image: "./resources/images/hero/bg_hero_2.png",
        title: "Реклама<span style=\"color: var(--c-green)\">.</span>",
        description: "Видеопроект:<br /><span class=\"b\">Азерчай</span>"
    },
    {
        image: "./resources/images/hero/bg_hero_3.png",
        title: "Документальное<br />кино<span style=\"color: var(--c-green)\">.</span>",
        description: "Видеопроект:<br /><span class=\"b\">Хой</span>"
    },
    {
        image: "./resources/images/hero/bg_hero_4.png",
        title: "TV формат<span style=\"color: var(--c-green)\">.</span>",
        description: "Видеопроект:<br /><span class=\"b\">Уроки русского</span>"
    },
    {
        image: "./resources/images/hero/bg_hero_5.png",
        title: "2D и 3D<br />графика<span style=\"color: var(--c-green)\">.</span>",
        description: "Видеопроект:<br /><span class=\"b\">Наука.Кооперация</span>"
    },
    {
        image: "./resources/images/hero/bg_hero_6.png",
        title: "Видеокомиксы<span style=\"color: var(--c-green)\">.</span>",
        description: "Видеопроект:<br /><span class=\"b\">Академия регби</span>"
    },
    {
        image: "./resources/images/hero/bg_hero_7.png",
        title: "Инфорграфика<br />и анимация<span style=\"color: var(--c-green)\">.</span>",
        description: "Видеопроект:<br /><span class=\"b\">Карантин для мусора</span>"
    },
];
var PROJECTS_LISTS_DATA = [
    [
        {
            url: "https://vimeo.com/542562001",
            image: "./resources/images/projects_previews/preview_01/1.png",
            title: "Сбербанк Ecoestate"
        },
        {
            url: "https://vimeo.com/542581074",
            image: "./resources/images/projects_previews/preview_01/2.png",
            title: "ФСК.ЖК"
        },
        {
            url: "https://vimeo.com/548331246",
            image: "./resources/images/projects_previews/preview_01/3.png",
            title: "Куда уходит мусор?"
        },
        {
            url: "https://vimeo.com/542560284",
            image: "./resources/images/projects_previews/preview_01/4.png",
            title: "Лесник"
        },
        {
            url: "https://vimeo.com/542669017",
            image: "./resources/images/projects_previews/preview_01/5.png",
            title: "Типовое жилье с Еленой Летучей"
        },
    ],
    [
        {
            url: "https://vimeo.com/542716931",
            image: "./resources/images/projects_previews/preview_02/1.png",
            title: "Андрей Никитин о Новгороде"
        },
        {
            url: "https://vimeo.com/542724006",
            image: "./resources/images/projects_previews/preview_02/2.png",
            title: "Михаил Казиник о музыке"
        },
        {
            url: "https://vimeo.com/542720565",
            image: "./resources/images/projects_previews/preview_02/3.png",
            title: "Евгений Миронов о театре"
        },
    ],
    [
        {
            url: "https://vimeo.com/542078150",
            image: "./resources/images/projects_previews/preview_03/1.png",
            title: "Азерчай"
        },
        {
            url: "https://vimeo.com/541601511",
            image: "./resources/images/projects_previews/preview_03/2.png",
            title: "Разделяй Москва"
        },
        {
            url: "https://vimeo.com/541599484",
            image: "./resources/images/projects_previews/preview_03/3.png",
            title: "Разделяй Москва"
        },
    ],
    [
        {
            url: "https://vimeo.com/542871490",
            image: "./resources/images/projects_previews/preview_04/2.png",
            title: "Без мусора в голове"
        },
        {
            url: "https://vimeo.com/543065549",
            image: "./resources/images/projects_previews/preview_04/1.png",
            title: "Хой"
        },
    ],
    [
        {
            url: "https://vimeo.com/542875346",
            image: "./resources/images/projects_previews/preview_05/1.png",
            title: "Уроки русского"
        },
        {
            url: "https://vimeo.com/543051817",
            image: "./resources/images/projects_previews/preview_05/3.png",
            title: "Демократия массового поражения"
        },
        {
            url: "https://vimeo.com/543048600",
            image: "./resources/images/projects_previews/preview_05/5.png",
            title: "Спортивный репортер"
        },
    ],
    [
        {
            url: "https://vimeo.com/542887801",
            image: "./resources/images/projects_previews/preview_06/2.png",
            title: "Наука. Инфрастуктура"
        },
        {
            url: "https://vimeo.com/542887778",
            image: "./resources/images/projects_previews/preview_06/3.png",
            title: "Наука. Кадры"
        },
        {
            url: "https://vimeo.com/542887754",
            image: "./resources/images/projects_previews/preview_06/1.png",
            title: "Наука. Кооперация"
        },
    ],
    [
        {
            url: "https://vimeo.com/542659348",
            image: "./resources/images/projects_previews/preview_07/1.png",
            title: "Академия регби"
        },
        {
            url: "https://vimeo.com/541604442",
            image: "./resources/images/projects_previews/preview_07/2.png",
            title: "Московские сезоны"
        },
        {
            url: "https://vimeo.com/542891634",
            image: "./resources/images/projects_previews/preview_07/3.png",
            title: "Динго и Тук-Тук"
        },
    ],
    [
        {
            url: "https://vimeo.com/542885022",
            image: "./resources/images/projects_previews/preview_08/1.png",
            title: "Карантин для мусора"
        },
        {
            url: "https://vimeo.com/542896899",
            image: "./resources/images/projects_previews/preview_08/2.png",
            title: "Оздоровление Волги"
        },
        {
            url: "https://vimeo.com/542896870",
            image: "./resources/images/projects_previews/preview_08/3.png",
            title: "Чистая вода"
        },
    ],
];
var TEAM_DATA = [
    {
        image: "./resources/images/team/1.png",
        name: "Сергей Чёрный",
        position: "Директор проектов"
    },
    {
        image: "./resources/images/team/2.png",
        name: "Димитр Тодоров",
        position: "CEO/Генеральный директор"
    },
    {
        image: "./resources/images/team/3.png",
        name: "Микаэл Карапетян",
        position: "Исполнительный продюсер"
    },
    {
        image: "./resources/images/team/4.png",
        name: "Ольга Панкратова",
        position: "Линейный продюсер"
    },
    {
        image: "./resources/images/team/5.png",
        name: "Алина Гудовская",
        position: "Линейный продюсер"
    },
    {
        image: "./resources/images/team/6.png",
        name: "Вероника Дрокова",
        position: "Менеджер проектов"
    },
    {
        image: "./resources/images/team/7.png",
        name: "Никита Колпаков",
        position: "Менеджер проектов"
    },
];
