var heroBlock = document.getElementById("hero");
var heroTitle = document.getElementById("hero-title");
var heroDescription = document.getElementById("hero-description");
var projectsMenu = document.getElementById("projects-menu");
var projectList = document.getElementById("project-list");
var teamContainer = document.getElementById("team-container");
var teamLeftArrow = document.querySelector(".team-arrow.left");
var teamRightArrow = document.querySelector(".team-arrow.right");
var menu = document.getElementById("menu");
var burger = document.querySelector(".menu-burger");
var closer = document.querySelector(".menu-closer");
var slideNumber = 0;
var activeData;
var isProjectsMenuOpen = false;
var timeOutIndex;
function setHero() {
    if (heroBlock != undefined && heroTitle != undefined && heroDescription != undefined) {
        slideNumber = activeData.length <= slideNumber ? 0 : slideNumber;
        heroBlock.style.backgroundImage = "url(\"" + activeData[slideNumber].image + "\")";
        heroTitle.innerHTML = "" + activeData[slideNumber].title;
        heroDescription.innerHTML = "" + activeData[slideNumber].description;
        if (!isProjectsMenuOpen) {
            timeOutIndex = setTimeout(function () {
                slideNumber++;
                setHero();
            }, 5000);
        }
        else
            clearTimeout(timeOutIndex);
    }
}
function setAllProjectsPreviewInroProjectsMenu() {
    var _loop_1 = function (i) {
        var localProjectList = projectsMenu.children[i].children[1];
        PROJECTS_LISTS_DATA[i].forEach(function (item) {
            localProjectList.appendChild(setProjectListItem(item));
        });
    };
    for (var i = 0; i < projectsMenu.children.length; i++) {
        _loop_1(i);
    }
}
function projectsMenuActions(menuItem) {
    var a = menuItem;
    if (!a.classList.contains("active")) {
        isProjectsMenuOpen = true;
        for (var i = 0; i < projectsMenu.children.length; i++) {
            var _i = projectsMenu.children[i].children[0];
            _i.classList.remove("active");
            _i.classList.add("unactive");
        }
        a.classList.add("active");
        a.classList.remove("unactive");
        slideNumber = +a.dataset.index;
        setProjectList();
        setTimeout(function () {
            document.body.scroll({
                left: 0,
                top: document.body.scrollTop + a.getBoundingClientRect().top - 60,
                behavior: "smooth"
            });
        }, 50);
    }
    else {
        isProjectsMenuOpen = false;
        for (var i = 0; i < projectsMenu.children.length; i++) {
            var _i = projectsMenu.children[i].children[0];
            _i.classList.remove("active");
            _i.classList.remove("unactive");
        }
        clearProjectList();
    }
    setHero();
}
function projectListener(e) {
    var projectsMenuLink = e.target;
    projectsMenuActions(projectsMenuLink);
}
function setProjectsMenuListeners() {
    if (projectsMenu != undefined) {
        for (var i = 0; i < projectsMenu.children.length; i++) {
            var _i = projectsMenu.children[i].children[0];
            _i.addEventListener("click", projectListener);
            _i.dataset.index = i.toString();
        }
    }
}
function setHeroByHash() {
    var menuItem = document.querySelector("a[href=\"" + location.hash + "\"]");
    if (menuItem != undefined)
        projectsMenuActions(menuItem);
}
function setProjectListItem(projItem) {
    var a = document.createElement("a");
    a.href = projItem.url;
    a.className = "project-list-item";
    a.target = "_blank";
    var img = document.createElement("span");
    img.className = "project-list-item-image";
    img.style.backgroundImage = "url(\"" + projItem.image + "\")";
    var title = document.createElement("span");
    title.className = "project-list-item-title b";
    title.textContent = projItem.title;
    a.appendChild(img);
    a.appendChild(title);
    return a;
}
function clearProjectList() {
    if (projectList.childNodes.length > 0) {
        for (var i = projectList.children.length; i > 0; i--) {
            projectList.children[i - 1].remove();
        }
    }
}
function setProjectList() {
    clearProjectList();
    for (var i = 0; i < PROJECTS_LISTS_DATA[slideNumber].length; i++) {
        projectList.appendChild(setProjectListItem(PROJECTS_LISTS_DATA[slideNumber][i]));
    }
}
function setTeamItem(itemData) {
    var item = document.createElement("div");
    item.className = "team-container-item";
    var image = document.createElement("div");
    image.className = "team-container-item-image";
    image.style.backgroundImage = "url(\"" + itemData.image + "\")";
    var name = document.createElement("span");
    name.className = "team-container-item-name b";
    name.textContent = itemData.name;
    var position = document.createElement("span");
    position.className = "team-container-item-position";
    position.textContent = itemData.position;
    teamContainer.appendChild(item);
    item.appendChild(image);
    item.appendChild(name);
    item.appendChild(position);
}
function teamPaging(e) {
    var firstItemBoundingBox = teamContainer.children[0].getBoundingClientRect();
    var itemWidth = firstItemBoundingBox.width;
    var margin = teamContainer.children[1].getBoundingClientRect().x - (itemWidth + firstItemBoundingBox.x);
    var parent = e.target;
    while (parent && parent.tagName != "svg")
        parent = parent.parentNode;
    if (parent.classList.contains("left")) {
        teamContainer.scroll({
            left: teamContainer.scrollLeft - (itemWidth + margin),
            top: 0,
            behavior: "smooth"
        });
    }
    else if (parent.classList.contains("right")) {
        teamContainer.scroll({
            left: teamContainer.scrollLeft + (itemWidth + margin),
            top: 0,
            behavior: "smooth"
        });
    }
}
function openMenu() {
    window.addEventListener("resize", closeMenu);
    document.body.style.overflow = "hidden";
    if (menu != undefined)
        menu.style.display = "flex";
}
function closeMenu() {
    document.body.removeAttribute("style");
    if (menu != undefined)
        menu.removeAttribute("style");
    window.removeEventListener("resize", closeMenu);
}
window.onload = function () {
    if (burger != undefined && closer != undefined) {
        burger.addEventListener("click", openMenu);
        closer.addEventListener("click", closeMenu);
    }
    if (location.pathname == "/" || location.pathname == "/index.html" || location.pathname == "/projects.html") {
        activeData = location.pathname == "/" || location.pathname == "/index.html" ? HERO_DATA_MAIN : HERO_DATA_PROJECTS;
        setHero();
        if (location.pathname == "/projects.html") {
            setAllProjectsPreviewInroProjectsMenu();
            setProjectsMenuListeners();
            if (location.hash != "")
                setHeroByHash();
        }
    }
    else if (location.pathname == "/team.html" && teamContainer != undefined && teamLeftArrow != undefined && teamRightArrow != undefined) {
        for (var i = 0; i < TEAM_DATA.length; i++)
            setTeamItem(TEAM_DATA[i]);
        teamLeftArrow.addEventListener("click", teamPaging);
        teamRightArrow.addEventListener("click", teamPaging);
    }
};
