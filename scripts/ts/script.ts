//import { IHeroData, IProjectListItemData, ITeam, HERO_DATA_MAIN, HERO_DATA_PROJECTS, PROJECTS_LISTS_DATA, TEAM_DATA } from "./data.js";

// НОДЫ
// hero
const heroBlock = <HTMLDivElement>document.getElementById("hero");
const heroTitle = <HTMLSpanElement>document.getElementById("hero-title");
const heroDescription = <HTMLSpanElement>document.getElementById("hero-description");
// projects menu
const projectsMenu = <HTMLUListElement>document.getElementById("projects-menu");
// project list
const projectList = <HTMLDivElement>document.getElementById("project-list");
// team block
const teamContainer = <HTMLDivElement>document.getElementById("team-container");
const teamLeftArrow = <SVGSVGElement>document.querySelector(".team-arrow.left");
const teamRightArrow = <SVGSVGElement>document.querySelector(".team-arrow.right");
// menu
const menu = <HTMLDivElement>document.getElementById("menu");
const burger = <SVGSVGElement>document.querySelector(".menu-burger");
const closer = <SVGSVGElement>document.querySelector(".menu-closer");

// индекс текущего слайда
let slideNumber: number = 0;
// актиные данные для hero
let activeData: IHeroData[];
// открыт ли проект (нажат ли пункт меню)
let isProjectsMenuOpen: boolean = false;
// индекс таймаута, чтобы останавливать автоматическую прокрутку слайдов
let timeOutIndex: number;

// установка рандомных картинок в hero
function setHero(): void {
	if (heroBlock != undefined && heroTitle != undefined && heroDescription != undefined) {
		slideNumber = activeData.length <= slideNumber ? 0 : slideNumber;
		heroBlock.style.backgroundImage = `url("${activeData[slideNumber].image}")`;
		heroTitle.innerHTML = `${activeData[slideNumber].title}`;
		heroDescription.innerHTML = `${activeData[slideNumber].description}`;
		if (!isProjectsMenuOpen) {
			timeOutIndex = setTimeout((): void => {
				slideNumber++;
				setHero();
			}, 5000);
		} else clearTimeout(timeOutIndex);
	}
}

// установка превью проектов внутрь списка меню проектов
function setAllProjectsPreviewInroProjectsMenu(): void {
	for (let i = 0; i < projectsMenu.children.length; i++) {
		let localProjectList: HTMLDivElement = projectsMenu.children[i].children[1] as HTMLDivElement;
		PROJECTS_LISTS_DATA[i].forEach((item): void => {
			localProjectList.appendChild(setProjectListItem(item));
		});
	}
}

// действия с меню проектов
function projectsMenuActions(menuItem: HTMLAnchorElement): void {
	let a: HTMLAnchorElement = menuItem;
	// если пункт меню не активен, то активируем
	if (!a.classList.contains("active")) {
		isProjectsMenuOpen = true;
		// деактивируем остальные пункты меню
		for (let i = 0; i < projectsMenu.children.length; i++) {
			let _i: HTMLAnchorElement = (projectsMenu.children[i] as HTMLLIElement).children[0] as HTMLAnchorElement
			_i.classList.remove("active");
			_i.classList.add("unactive");
		}
		a.classList.add("active");
		a.classList.remove("unactive");
		slideNumber = +a.dataset.index;
		setProjectList();
		setTimeout((): void => {
			document.body.scroll({
				left: 0,
				top: document.body.scrollTop + a.getBoundingClientRect().top - 60,
				behavior: "smooth",
			});
		}, 50);
	// если пункт меню активен, то деактивируем
	} else {
		isProjectsMenuOpen = false;
		for (let i = 0; i < projectsMenu.children.length; i++) {
			let _i: HTMLAnchorElement = (projectsMenu.children[i] as HTMLLIElement).children[0] as HTMLAnchorElement
			_i.classList.remove("active");
			_i.classList.remove("unactive");
		}
		clearProjectList();
	}
	setHero();
}

// листнер пункта меню проектов
function projectListener(e: MouseEvent): void {
	let projectsMenuLink: HTMLAnchorElement = e.target as HTMLAnchorElement
	projectsMenuActions(projectsMenuLink);
}

// установка листнеров на меню проектов
function setProjectsMenuListeners(): void {
	if (projectsMenu != undefined) {
		for (let i = 0; i < projectsMenu.children.length; i++) {
			let _i: HTMLAnchorElement = (projectsMenu.children[i] as HTMLLIElement).children[0] as HTMLAnchorElement
			_i.addEventListener("click", projectListener);
			_i.dataset.index = i.toString();
		}
	}
}

// открытие проектов по хэшу при загрузке страницы
function setHeroByHash(): void {
	let menuItem: HTMLAnchorElement = document.querySelector(`a[href="${location.hash}"]`);
	if (menuItem != undefined) projectsMenuActions(menuItem);
}

// установка превью проекта
function setProjectListItem(projItem: IProjectListItemData): HTMLAnchorElement {
	let a: HTMLAnchorElement = document.createElement("a");
	a.href = projItem.url;
	a.className = "project-list-item";
	a.target = "_blank";
	let img: HTMLSpanElement = document.createElement("span");
	img.className = "project-list-item-image";
	img.style.backgroundImage = `url("${projItem.image}")`;
	let title: HTMLSpanElement = document.createElement("span");
	title.className = "project-list-item-title b";
	title.textContent = projItem.title;
	a.appendChild(img);
	a.appendChild(title);
	return a;
}

// чистка превью из списка проектов
function clearProjectList(): void {
	if (projectList.childNodes.length > 0) {
		for (let i = projectList.children.length; i > 0; i--) {
			projectList.children[i - 1].remove();
		}
	}
}

// установка всех превью раздела поектов
function setProjectList(): void {
	// сначала чистим контейнер
	clearProjectList();
	// добавляем новые превьюхи
	for (let i = 0; i < PROJECTS_LISTS_DATA[slideNumber].length; i++) {
		projectList.appendChild(setProjectListItem(PROJECTS_LISTS_DATA[slideNumber][i]));
	}
}

// создание члена команды
function setTeamItem(itemData: ITeam): void {
	let item: HTMLDivElement = document.createElement("div");
	item.className = "team-container-item";
	let image: HTMLDivElement = document.createElement("div");
	image.className = "team-container-item-image";
	image.style.backgroundImage = `url("${itemData.image}")`;
	let name: HTMLSpanElement = document.createElement("span");
	name.className = "team-container-item-name b";
	name.textContent = itemData.name;
	let position: HTMLSpanElement = document.createElement("span");
	position.className = "team-container-item-position";
	position.textContent = itemData.position;
	teamContainer.appendChild(item);
	item.appendChild(image);
	item.appendChild(name);
	item.appendChild(position);
}

// пейджинг команды
function teamPaging(e: MouseEvent): void {
	const firstItemBoundingBox: DOMRect = (teamContainer.children[0] as HTMLDivElement).getBoundingClientRect();
	const itemWidth: number = firstItemBoundingBox.width;
	const margin: number = (teamContainer.children[1] as HTMLDivElement).getBoundingClientRect().x - (itemWidth + firstItemBoundingBox.x)
	let parent: SVGSVGElement = e.target as SVGSVGElement
	while (parent && parent.tagName != "svg") parent = parent.parentNode as SVGSVGElement;
	if (parent.classList.contains("left")) {
		teamContainer.scroll({
			left: teamContainer.scrollLeft - (itemWidth + margin),
			top: 0,
			behavior: "smooth",
		});
	} else if (parent.classList.contains("right")) {
		teamContainer.scroll({
			left: teamContainer.scrollLeft + (itemWidth + margin),
			top: 0,
			behavior: "smooth",
		});
	}
}

// открытие меню
function openMenu(): void {
	window.addEventListener("resize", closeMenu);
	document.body.style.overflow = "hidden";
	if (menu != undefined) menu.style.display = "flex";
}
// закрытие меню
function closeMenu(): void {
	document.body.removeAttribute("style");
	if (menu != undefined) menu.removeAttribute("style");
	window.removeEventListener("resize", closeMenu);
}

// тут все подгружается
window.onload = (): void => {
	// меню
	if (burger != undefined && closer != undefined) {
		burger.addEventListener("click", openMenu);
		closer.addEventListener("click", closeMenu);
	}
	// если главная или проекты
	if (location.pathname == "/" || location.pathname == "/index.html" || location.pathname == "/projects.html") {
		activeData = location.pathname == "/" || location.pathname == "/index.html" ? HERO_DATA_MAIN : HERO_DATA_PROJECTS;
		setHero();
		if (location.pathname == "/projects.html") {
			setAllProjectsPreviewInroProjectsMenu();
			setProjectsMenuListeners();
			if (location.hash != "") setHeroByHash();
		}
	} else if (location.pathname == "/team.html" && teamContainer != undefined && teamLeftArrow != undefined && teamRightArrow != undefined) {
		for (let i = 0; i < TEAM_DATA.length; i++) setTeamItem(TEAM_DATA[i]);
		teamLeftArrow.addEventListener("click", teamPaging)
		teamRightArrow.addEventListener("click", teamPaging)
	}
}

